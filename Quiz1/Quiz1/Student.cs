﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1
{
    class Student
    {
        private string _Name;
        private int _IDnum;
        private bool _IsNewEnrollee;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public int Idnum
        {
            get { return _IDnum; }
            set { _IDnum = value; }
        }
        public void SetID(int id)
        {
            _IDnum = id;
        }

        public bool IsEnrollee
        {
            get { return _IsNewEnrollee; }
            set { _IsNewEnrollee = value; }
        }

        public void ChecksEnrollee(bool enrollee)
        {
            _IsNewEnrollee = enrollee;
        }

        public bool CheckingInfo(string status)
        {
            if (_Name == " " || _IDnum <= 0 || status == " ")
            {
                return false;
            }
            return true;
        }
    }
}
