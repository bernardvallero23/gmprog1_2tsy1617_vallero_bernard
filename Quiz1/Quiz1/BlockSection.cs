﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1
{
    class BlockSection
    {
        private string _CourseCode;
        private int _SchoolYear;
        private int _Term;
        private int _BlockLimit;
        private int _currentStudents;
        private Student[] Students;

        public BlockSection()
        {
            _currentStudents = 0;
        }

        public string CourseCode
        {
            get { return _CourseCode; }
            set { _CourseCode = value; }
        }
        public int SchoolYear
        {
            get { return _SchoolYear; }
            set { _SchoolYear = value; }
        }

        public int Term
        {
            get { return _Term; }
            set { _Term = value; }
        }
        public void BlockLimit(int size)
        {
            _BlockLimit = size;
            Students = new Student[size];
        }
        public int GetBlockLimit()
        {
            return _BlockLimit;
        }
        public void AddStudent(Student student)
        {
            Students[_currentStudents] = student;
            _currentStudents++;
        }
        public int GetCurrentEnrolled()
        {
            return _currentStudents;
        }
        public bool CheckIfBlockFull()
        {
            if (_currentStudents == _BlockLimit)
            {
                return true;
            }
            return false;

        }

        public void PrintInfo()
        {
            Console.WriteLine("Course Code: " +  _CourseCode);
            Console.WriteLine("School year:" + _SchoolYear);
            Console.WriteLine("Term:" + _Term);
            Console.WriteLine("New Enrollees:");

            foreach(Student student in Students)
            {
                if(student == null) { continue; }
                if (student.IsEnrollee == false) { continue; }
                Console.WriteLine("{0}", student.Name);
            }
            Console.WriteLine("Enrollees:");
            foreach(Student student in Students)
            {
                if(student == null) { continue; }
                if (student.IsEnrollee == false) { continue; }
                Console.WriteLine("{0}", student.Name);
            }

        }

        public void CheckStudentbyName(string name)
        {
            foreach(Student student in Students)
            {
                if(student.Name == name)
                {
                    Console.WriteLine("Student is Enrolled");
                    return;
                }
                
            }
        }

        public void CheckStudentbyIdnum(int number)
        {
            foreach(Student student in Students)
            {
                if(student.Idnum == number)
                {
                    Console.WriteLine("Student is Enrolled");
                    return;
                }
            }
        }

        public void FillingUpInfo()
        {
            Console.WriteLine("Course Code:");
            CourseCode = Console.ReadLine();

            Console.WriteLine("School Year:");
            SchoolYear = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Term:");
            Term = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Class Size:");
            int size = Int32.Parse(Console.ReadLine());
            BlockLimit(size);

            for(;;)
            {
                if (CheckIfBlockFull())
                {
                    PrintInfo();
                    break;
                }
                else
                {
                    int currentEnrolled = GetCurrentEnrolled();
                    for (int i = currentEnrolled; i < GetBlockLimit(); i++)
                    {
                        Student student = new Student();
                        Console.WriteLine("Name:");
                        student.Name = Console.ReadLine();

                        Console.WriteLine("ID Number:");
                        string number = Console.ReadLine();
                        if (number != null || number != " " )
                        {
                            student.SetID(Int32.Parse(number));
                        }
                      

                        Console.WriteLine("Are you a new Enrollee)(y/n):");
                        string choice = Console.ReadLine();
                        switch(choice)
                        {
                            case "y":
                            case "Y":
                                student.ChecksEnrollee(true);
                                break;

                            case "n":
                            case "N":
                                student.ChecksEnrollee(false);
                                break;
                        }
                        if(student.CheckingInfo(choice))
                        {
                            AddStudent(student);
                        }
                        else
                        {
                            Console.WriteLine("Info incomplete!");
                            break;
                        }
                    }
                }

            }
            Console.WriteLine("[a]search by name [b]search by idnum?");
            string output = Console.ReadLine();
            switch (output)
            {
                case "A":
                case "a":
                    Console.WriteLine("Name of student you want to search:");
                    string name = Console.ReadLine();
                    CheckStudentbyName(name);
                    break;

                case "B":
                case "b":
                    Console.WriteLine("Idnumber of student you want to search:");
                    int idnum = Int32.Parse(Console.ReadLine());
                    CheckStudentbyIdnum(idnum);
                    break;
            }

            Console.WriteLine("Do you want to create another block?Y/N");
            string input = Console.ReadLine();
            switch (input)
            {
                case "y":
                case "Y":
                    FillingUpInfo();
                    break;
                case "n":
                case "N":
                    Console.WriteLine("Thank you!");
                    break;
            }
        }
    }
}
