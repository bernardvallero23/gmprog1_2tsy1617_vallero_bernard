﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Character
    {
        public enum Type { playable, npc }
        public enum Class { Wizard, Prince, Elf, Ogre, Dragon }

        protected Type CharacterType;
        protected Class CharacterClass;
        protected int HP = 0;
        protected int ATK = 0;
        protected bool VIS = true;

        public virtual void Greet() {

        }

        public virtual void Taunt()
        {

        }

        public virtual void Win()
        {

        }

        public void PrintStats() {
            Console.WriteLine("-- " + CharacterClass.ToString() + " Stats " + "HP: " + HP + " ATK: " + ATK);
        }

        public string GetClass() {
            return CharacterClass.ToString();
        }

        //Automatic, Name before Dialougue
        protected void Dialogue(string dialogue) {
            Console.WriteLine(CharacterClass.ToString() + ": " + dialogue);
        }

        //Item effect implemented
        public void AcceptedItem(int hp, int atk, bool vis) {
            HP += hp;
            ATK += atk;
        }

        public int GetHP() {
            return HP;
        }

        public int GetATK() {
            return ATK;
        }

        public bool GetVIS() {
            return VIS;
        }

        public void Hit(int hit) {
            HP -= hit;
        }
    }
}
