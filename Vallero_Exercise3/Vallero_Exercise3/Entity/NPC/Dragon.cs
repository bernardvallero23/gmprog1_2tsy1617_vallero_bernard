﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Dragon : Character
    {
        public Dragon() {
            CharacterType = Type.npc;
            CharacterClass = Class.Dragon;
            HP = 75;
            ATK = 30;
        }

        public override void Taunt()
        {
            base.Taunt();
            Dialogue("Who dares disturb my slumber! Death will come to you!");
        }

        public override void Win()
        {
            base.Win();
            Dialogue("Inferior being. Now, I’m back to sleep.");
        }
    }
}
