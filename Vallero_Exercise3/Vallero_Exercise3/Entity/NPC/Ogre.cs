﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Ogre : Character
    {
        public Ogre()
        {
            CharacterType = Type.npc;
            CharacterClass = Class.Ogre;
            HP = 20;
            ATK = 20;
        }

        public override void Taunt()
        {
            base.Taunt();
            Dialogue("Ah! Dinner!");
        }

        public override void Win()
        {
            base.Win();
            Dialogue("HAHAH! Lame creature!");
        }
    }
}
