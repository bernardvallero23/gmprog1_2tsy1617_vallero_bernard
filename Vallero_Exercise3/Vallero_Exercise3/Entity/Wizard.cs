﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Wizard : Character
    {
        public Wizard()
        {
            CharacterType = Type.playable;
            CharacterClass = Class.Wizard;
            HP = 20;
            ATK = 15;
        }

        public override void Greet()
        {
            base.Greet();
            Dialogue("Magic comes with a price mi Lady!");
        }
    }
}
