﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Elf : Character
    {
        public Elf() {
            CharacterType = Type.playable;
            CharacterClass = Class.Elf;
            HP = 15;
            ATK = 30;
        }

        public override void Greet()
        {
            base.Greet();
            Dialogue("At your service Mistress!");
        }
    }
}
