﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Prince : Character
    {
        public Prince()
        {
            CharacterType = Type.playable;
            CharacterClass = Class.Prince;
            HP = 15;
            ATK = 30;
        }

        public override void Greet()
        {
            base.Greet();
            Dialogue("Wait for me my princess!");
        }
    }
}
