﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class GameManager
    {
        List<Character> ListPlayable = new List<Character>();
        List<Character> ListNPC = new List<Character>();
        List<Item> ListItems = new List<Item>();
        List<Item> ListDebris = new List<Item>();

        Random rnd = new Random();

        Character Hero;
        Character Enemy;
        Item Loot;
        Item Debris;
        bool Wins;

        public GameManager() {
           
            Init();
            
            PrintPrincessLine();
            
            Hero = CreateHero();
            Hero.Greet();
        
            PrintLine("-- on your way you saw something shiny");
            Loot = CreateLoot();
            Loot.Stat();
            Loot.UseItem(Hero);
            Hero.PrintStats();
            PrintPrincessLine();
            PrintLine("-- in the castle you encounter a monster");
            if (Hero.GetVIS())
            {
                
                Enemy = CreateEnemy();
                Enemy.Taunt();
                Enemy.PrintStats();
                PrintLine("-- you attack the gigantic monster");
                Wins = ActionHeroWins();
                if (Wins == true)
                {
                    PrintHeroWins();
                }
                else
                {
                    PrintHeroLose();
                }
            }
            else {
                Wins = true;
                PrintLine("-- in the castle you saw the monster");
                PrintLine("--with the Great Clock you snuck in quietly and got the princess out");
                PrintLine("--you exit the castle got the princess to safety!");
                PrintLine("Princess: My hero!");
            }


            if (Wins == true) {
                Debris = CreateDebris();
                Debris.UseItem(Hero);
                PrintLine("-- unfortunately, a " + Debris.Name.ToString() + " falls on your head ");
                Hero.PrintStats();
                Wins = ActionHeroFate();
                if (Wins == true)
                {
                    PrintLine("-- it didn’t hurt much, you lived happily ever after!");
                }
                else {
                    PrintLine("-- you are so unlucky you died!");
                }
            }

        }

        private void PrintHeroWins() {
            PrintLine("-- " + Enemy.GetClass() + " wimps in pain and faints.");
            PrintLine("-- you exit the castle got the princess to safety!");
        }

        private void PrintHeroLose() {
            PrintLine("-- " + Enemy.GetClass() + " laughs in vain!");
            Enemy.Win();
            PrintLine("-- you lost.");
        }

        private void PrintPrincessLine() {
            PrintLine("Princess: Help!");
            PrintLine("-- a princess yelps in a castle not far");
        }

        private void PrintLine(string line) {
            Console.WriteLine(line);
        }

        private void Init(){
            Elf elf = new Elf();
            ListPlayable.Add(elf);

            Wizard wiz = new Wizard();
            ListPlayable.Add(wiz);

            Prince pri = new Prince();
            ListPlayable.Add(pri);

            Dragon dra = new Dragon();
            ListNPC.Add(dra);

            Ogre ogr = new Ogre();
            ListNPC.Add(ogr);

            Elixir eli = new Elixir();
            ListItems.Add(eli);

            GreatCloak gre = new GreatCloak();
            ListItems.Add(gre);

            Rat rat = new Rat();
            ListDebris.Add(rat);

            Plate pla = new Plate();
            ListDebris.Add(pla);

            Rock roc = new Rock();
            ListDebris.Add(roc);
        }

     
        private Character CreateHero() {
            int idx = rnd.Next(0, ListPlayable.Count());
            return ListPlayable[idx];
        }

   
        private Character CreateEnemy()
        {
            int idx = rnd.Next(0, ListNPC.Count());
            return ListNPC[idx];
        }

        
        private Item CreateLoot()
        {
            int idx = rnd.Next(0, ListItems.Count());
            return ListItems[idx];
        }

      
        private Item CreateDebris()
        {
            int idx = rnd.Next(0, ListDebris.Count());
            return ListDebris[idx];
        }

        private bool ActionHeroWins() {
            Hero.Hit(Enemy.GetATK());
            Enemy.Hit(Hero.GetATK());

            Hero.PrintStats();
            Enemy.PrintStats();

            if (Hero.GetHP() <= 0 || Hero.GetHP() < Enemy.GetHP()) {
                
                return false;
            }

            return true;
        }

        private bool ActionHeroFate() {
            if (Hero.GetHP() <= 0) {
                return false;
            }
            return true;
        }
    }
}
