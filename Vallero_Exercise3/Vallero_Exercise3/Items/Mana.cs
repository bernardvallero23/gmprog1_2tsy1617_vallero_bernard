﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Mana : Item
    {
        public Mana()
        {
            Name = ItemClass.Mana;
        }

        public override void Stat()
        {
            base.Stat();
            Console.WriteLine("multiplies ATK by 3");
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(0, hero.GetATK() * 3, true);
        }
    }
}
