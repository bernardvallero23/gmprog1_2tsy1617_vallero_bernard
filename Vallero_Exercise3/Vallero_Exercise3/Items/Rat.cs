﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Rat : Item
    {
        public Rat()
        {
            Name = ItemClass.Rat;
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(-30, 0, true);
        }
    }
}
