﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Plate : Item
    {
        public Plate()
        {
            Name = ItemClass.Plate;
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(-10, 0, true);
        }
    }
}
