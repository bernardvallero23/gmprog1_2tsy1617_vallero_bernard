﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class GreatCloak : Item
    {
        public GreatCloak()
        {
            Name = ItemClass.GreatCloak;
        }

        public override void Stat()
        {
            base.Stat();
            Console.WriteLine("makes you invisible.");
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(0, 0, false);
        }
    }
}
