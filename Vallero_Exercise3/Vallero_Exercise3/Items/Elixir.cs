﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Elixir : Item
    {
        public Elixir() {
            Name = ItemClass.Elixir;
        }

        public override void Stat()
        {
            base.Stat();
            Console.WriteLine("adds 50 HP");
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(50, 0, true);
        }
    }
}
