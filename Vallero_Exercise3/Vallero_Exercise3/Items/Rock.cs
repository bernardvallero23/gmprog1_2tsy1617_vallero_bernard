﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Rock : Item
    {
        public Rock()
        {
            Name = ItemClass.Rock;
        }

        public override void UseItem(Character hero)
        {
            base.UseItem(hero);
            hero.AcceptedItem(-20, 0, true);
        }
    }
}
