﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise3
{
    class Item
    {
        public enum ItemClass { Elixir, Mana, GreatCloak, Rock, Plate, Rat};
        public ItemClass Name;

        public virtual void Stat() {
            Console.Write("-- you got an " + Name.ToString() + ", ");
        }

        public virtual void UseItem(Character hero) {
          
        }
    }
}
