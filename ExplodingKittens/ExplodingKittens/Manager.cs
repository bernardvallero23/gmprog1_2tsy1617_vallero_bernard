﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Manager
    {
        List<Cards.Card> deck = new List<Cards.Card>();

        List<Player> PlayersInRound = new List<Player>();        
        Random rng = new Random();

        int round = 1;

        public Manager()
        {
            Deck deck1 = new Deck();          

            Console.WriteLine("Press any key to continue..");
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("Printing deck");
            deck1.ViewAllDeck();
            Console.WriteLine("  ");
            Console.WriteLine("Press any key to continue..");
            Console.ReadKey();
            Console.Clear();

            AddPlayers();
            Console.WriteLine("Dealing cards");
            AssignCards(deck1.GetDeck());
            deck1.AddExplodingkitten();

            Console.WriteLine("Printing deck");
            deck1.ViewAllDeck();

            deck = deck1.GetDeck();

            GameLoop();

        }

        void AddPlayers()
        {
            Player P1 = new Player();
            Player P2 = new Player();
            Player P3 = new Player();
            Player P4 = new Player();
            Player P5 = new Player();

            PlayersInRound.Add(P1);
            PlayersInRound.Add(P2);
            PlayersInRound.Add(P3);
            PlayersInRound.Add(P4);
            PlayersInRound.Add(P5);
        }
        
        private void AssignCards(List<Cards.Card> RD)
        {
            for (int i = 0; i < PlayersInRound.Count(); i++)
            {
                List<Cards.Card> hand = new List<Cards.Card>();
                for (int x = 0; x < 4; x++)
                {
                    int r = rng.Next(RD.Count());
                    hand.Add(RD[r]);
                    RD.RemoveAt(r);
                }
                Cards.Defuse def = new Cards.Defuse();
                hand.Add(def);
                deck = deck.OrderBy(x => rng.Next()).ToList();

                PlayersInRound[i].AssignHand(hand);
            }
            Console.WriteLine("Deck has {0} cards left", RD.Count());


        }       

        void GameLoop()
        {
            Console.WriteLine("Press any key to continue..");
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine("You're about to play Exploding Kittens in a while");
            for (;;)
            {
                Console.WriteLine("Round {0}", round);
                round++;
                for (int i = 0; i < PlayersInRound.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            Console.WriteLine("Player's turn");
                            PlayersInRound[i].AssignDeck(deck);
                            PlayersInRound[i].ViewHand();
                            PlayersInRound[i].PlayerPlay(PlayersInRound);
                            PlayersInRound[i].AddCard(DrawnCard());
                            CheckWin(PlayersInRound[i].GetHand(), deck);
                            break;

                        default:
                            Console.WriteLine("AI's turn");
                            PlayersInRound[i].AssignDeck(deck);
                            PlayersInRound[i].NPCPlay(i);
                            PlayersInRound[i].AddCard(DrawnCard());
                            CheckWin(PlayersInRound[i].GetHand(), deck);
                            break;
                    }
                }  
            }
        }
        Cards.Card DrawnCard()
        {
            return deck[0];
        }

        void CheckWin(List<Cards.Card> hand, List<Cards.Card> deck)
        {
            Cards.Card last = hand[hand.Count - 1];

            if (last is Cards.ExplodingKitten)
            {
                Console.WriteLine("This player has drawn an Exploding Kitten");

               for (int i = 0; i < hand.Count(); i++)
               {
                   if(hand[i] is Cards.ExplodingKitten)
                   {
                       Console.WriteLine("Drawn an Exploding Kitten");
                       for (int x = 0; x < hand.Count(); x++)
                       {
                           if (hand[x] is Cards.Defuse)
                           {
                               Console.WriteLine("Defused!");
                               hand.RemoveAt(x);
                               break;

                           }
                           else
                           {
                               Console.WriteLine("LOSE");
                               Environment.Exit(0);                               
                           }
                       }
                       deck.Insert(rng.Next(deck.Count()), hand[i]);
                   }
               }
            }    
        }
    }
}
