﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum CC
{
    Taco,
    Watermelon,
    BrownCat,
    Ninja,
    Rainbow
}

namespace ExplodingKittens.Cards
{
    class Card
    {
        protected string name;

        public CC CatCardType;

        public virtual string GetName()
        {
            return name;
        }

        public virtual void Effect(int c, List<Cards.Card> deck, Player pl, Player Re)
        {
            Console.WriteLine("Effect");
        }
    }

    
}
