﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Player
    {
        public List<Cards.Card> Hand = new List<Cards.Card>();
        private List<Cards.Card> deck = new List<Cards.Card>();
        private List<Player> Players = new List<Player>();
        

        Random rnd = new Random();

        public Player()
        {
            Console.WriteLine("Player 1");
        }

        public void ViewHand()
        {
            Console.WriteLine("===================");
            Console.WriteLine("Available Cards: ");
            for (int i = 0; i < Hand.Count(); i++)
            {
                Console.Write("[{0}]. ", i);
                Console.WriteLine(Hand[i].GetName());               
            }
            Console.WriteLine("===================");
        }

        public void AssignHand(List<Cards.Card> hand)
        {
            Hand = hand;
        }

        public void PlayerPlay(List<Player> player)
        {
            Players = player;

            Console.WriteLine("Would you like to play a card?");
            Console.WriteLine("[1]Yes or [2]No");
            int Choice = Convert.ToInt32(Console.ReadLine());

            if (Choice == 1)
            {
                Console.WriteLine("What card would you like to play?");
                int number = Convert.ToInt32(Console.ReadLine());

                Hand[number].Effect(1,deck, Players[1], Players[0]);
                Hand.RemoveAt(number);

            }             
           
            Console.WriteLine("Press any key to continue..");
            Console.ReadKey();
        }

        public void NPCPlay(int i)
        {
            int r = rnd.Next(100);
            int f = rnd.Next(Hand.Count());
            if (r <= 50)
            {
                Console.WriteLine("Opponent is playing a card!");
                //Hand[f].Effect(2,deck, PL[rng.Next(PL.Count())],PL[i]);
                Hand.RemoveAt(f);
            }          
        }

        public void AddCard(Cards.Card ncard)
        {
            Hand.Add(ncard);
        }

        public List<Cards.Card> GetHand()
        {
            return Hand;
        }

        public void AssignDeck(List<Cards.Card> f)
        {
            deck = f;
        }

        public Cards.Card GetRanCard()
        {
            return Hand[rnd.Next(Hand.Count())];
        }
    }
}
