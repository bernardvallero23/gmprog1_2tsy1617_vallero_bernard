﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens.Cards
{
    class Shuffle : Cards.Card
    {
        Random rnd = new Random();

        public Shuffle()
        {
            name = "Shuffle";
        }

        public override void Effect(int c, List<Card> deck, Player pl, Player Re)
        {
            deck = deck.OrderBy(x => rnd.Next()).ToList();

        }
    }
}
