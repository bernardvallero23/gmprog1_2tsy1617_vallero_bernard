﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens.Cards
{
    class Favor : Cards.Card
    {
        public Favor()
        {
            name = "Favor";
        }

        public override void Effect(int c, List<Card> deck, Player give, Player Re)
        {
            Re.AddCard(give.GetRanCard());
        }

    }
}
