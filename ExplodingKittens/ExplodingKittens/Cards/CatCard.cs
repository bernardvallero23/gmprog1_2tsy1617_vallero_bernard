﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens.Cards
{
    class CatCard : Cards.Card
    {
        private string s;

        public CatCard(CC T)
        {
            AssignType(T);

            name = s + " Cat Card";
        }


        public void AssignType(CC type)
        {
            CatCardType = type;

            switch (CatCardType)
            {
                case CC.Taco:
                    s = "Taco";
                    break;

                case CC.Watermelon:
                    s = "Watermelon";
                    break;

                case CC.BrownCat:
                    s = "BrownCat";
                    break;

                case CC.Ninja:
                    s = "Ninja";
                    break;

                case CC.Rainbow:
                    s = "Rainbow";
                    break;
            }

        }
    }
}
