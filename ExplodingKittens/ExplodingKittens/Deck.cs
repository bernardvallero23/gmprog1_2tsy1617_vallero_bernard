﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Deck
    {
        List<Cards.Card> MainDeck = new List<Cards.Card>();

        public Deck()
        {
            Console.WriteLine("Deck Creating");
            AddCards();         
        }

        public void AddCards()
        {
            for (int i = 0; i < 5; i++)
            {
                Cards.Nope Nope = new Cards.Nope();
                MainDeck.Add(Nope);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.Attack Attack = new Cards.Attack();
                MainDeck.Add(Attack);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.Skip Skip = new Cards.Skip();
                MainDeck.Add(Skip);
            }
            for (int i = 0; i < 4; i++)
            {
                Cards.Favor Favor = new Cards.Favor();
                MainDeck.Add(Favor);
            }
            for (int i = 0; i < 4; i++)
            {
                Cards.Shuffle Shuffle = new Cards.Shuffle();
                MainDeck.Add(Shuffle);
            }
            for (int i = 0; i < 4; i++)
            {
                Cards.Future Future = new Cards.Future();
                MainDeck.Add(Future);
            }

            for (int i = 0; i < 4; i++)//cc1 4 times
            {
                Cards.CatCard Taco = new Cards.CatCard(CC.Taco);
                MainDeck.Add(Taco);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.CatCard Watermelon = new Cards.CatCard(CC.Watermelon);
                MainDeck.Add(Watermelon);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.CatCard Browncat = new Cards.CatCard(CC.BrownCat);
                MainDeck.Add(Browncat);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.CatCard Ninja = new Cards.CatCard(CC.Ninja);
                MainDeck.Add(Ninja);
            }

            for (int i = 0; i < 4; i++)
            {
                Cards.CatCard Rainbow = new Cards.CatCard(CC.Rainbow);
                MainDeck.Add(Rainbow);
            }
        }


        public void ViewAllDeck()
        {
            Console.WriteLine("Displaying Cards");

            for (int i = 0; i < MainDeck.Count(); i++)
            {
                Console.WriteLine("[]{0}",MainDeck[i].GetName());                
            }

            Console.WriteLine("There are {0} cards in your deck", MainDeck.Count());
        }

        public List<Cards.Card> GetDeck()
        {
            return MainDeck;
        }

        public void AddExplodingkitten()
        {
            for (int i = 0; i < 4; i++)
            {
                Cards.ExplodingKitten add = new Cards.ExplodingKitten();
                MainDeck.Add(add);
            }
        }

        public void SeeFuture()
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(MainDeck[i].GetName());
            }

        }
    }
}
