﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm_gmprog
{
    class Fleet
    {
        private string _FleetName;
        public string FleetName
        {
            get { return _FleetName; }
            set { _FleetName = value; }
        }

        private int _FleetSpeed;
        public int FleetSpeed
        {
            get { return _FleetSpeed; }
            set { _FleetSpeed = value; }
        }

        private int FleetCount;

        private List<Starship> ListStarships = new List<Starship>();

        public List<Starship> GetListStarships()
        {
            return ListStarships;
        }

        public void AddStarships(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Starship ship = new Starship();
                FleetCount++;
                ship.Name = FleetName + FleetCount;
                ListStarships.Add(ship);
            }
        }

        public void RemoveStarships(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Starship ship = ListStarships[i];
                ListStarships.Remove(ship);       
            }
        }
        public void PrintStarshipNames()
        {
            Console.WriteLine("=============================");
            Console.WriteLine("FleetName:{0}", FleetName);
            foreach (Starship unit in ListStarships)
            {
                Console.WriteLine("{0}, The speed is {1}", unit.Name,_FleetSpeed);
            }
            Console.WriteLine("=============================");
        }
    }
}
