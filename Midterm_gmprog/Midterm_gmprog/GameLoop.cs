﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm_gmprog
{
    class GameLoop

    {
        public List<Fleet> ListFleets = new List<Fleet>();

        public void PrintMenuText()
        {
            Console.WriteLine("=================================");
            Console.WriteLine("[1] Create a Fleet");
            Console.WriteLine("[2] Display all Existing Fleets");
            Console.WriteLine("[3] Add More Starships to Fleet");
            Console.WriteLine("[4] Remove Starships from Fleet");
            Console.WriteLine("[5] Display Fleet Info");
            Console.WriteLine("[6] Remove or Delete Fleet");
            Console.WriteLine("=================================");

            int choice = Int32.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Fleet fleet1 = new Fleet();
                    Console.WriteLine("Fleet Name:");
                    fleet1.FleetName = Console.ReadLine();

                    Console.WriteLine("Fleet Speed:");
                    fleet1.FleetSpeed = Int32.Parse(Console.ReadLine());

                    Console.WriteLine("Ship Number:");
                    int shipnumber= int.Parse(Console.ReadLine());
                    fleet1.AddStarships(shipnumber);
                    AddFleet(fleet1);
                    break;
                case 2:
                    PrintFleetNames();
                    break;
                case 3:
                    Console.WriteLine("Name of Fleet that ypu want to add a Starships:");
                    string FleetName = Console.ReadLine();
                    Fleet ToAddShips = GetFleetWithName(FleetName);
                    if (ToAddShips == null)
                    {
                        Console.WriteLine("Fleet does not exist!");
                    }
                    else {
                        Console.WriteLine("Number of Ships to Add:");
                        int addShipsCount = Int32.Parse(Console.ReadLine());
                        ToAddShips.AddStarships(addShipsCount);
                        ToAddShips.PrintStarshipNames();
                    }
                    break;
                case 4:
                    Console.WriteLine("Name of fleet that you want to remove a starships:");
                    FleetName = Console.ReadLine();
                    Fleet ToSubtract = GetFleetWithName(FleetName);
                    if (ToSubtract == null)
                    {
                        Console.WriteLine("Fleet does not exist!");
                    }
                    else {
                        Console.WriteLine("Number of Ships to Remove:");
                        int removeShips = Int32.Parse(Console.ReadLine());
                        ToSubtract.RemoveStarships(removeShips);
                        ToSubtract.PrintStarshipNames();
                    }
                    break;
                case 5:
                    Console.WriteLine("Name of Fleet that you want to display:");
                    FleetName = Console.ReadLine();
                    Fleet ToPrint = GetFleetWithName(FleetName);
                    if (ToPrint == null)
                    {
                        Console.WriteLine("Fleet does not exist!");
                    }
                    else {
                        ToPrint.PrintStarshipNames();
                    }
                    break;
                case 6:
                    Console.WriteLine("Name of fleet that you want to remove:");
                    FleetName = Console.ReadLine();
                    Fleet ToRemove = GetFleetWithName(FleetName);
                    RemoveFleet(ToRemove);
                    PrintFleetNames();
                    break;
            }


        }
        public void AddFleet(Fleet unit)
        {
            ListFleets.Add(unit);
        }

        public void RemoveFleet(Fleet unit)
        {
            if (ListFleets.Contains(unit) == true)
            {
                ListFleets.Remove(unit);
            }
        }

        public void PrintFleetNames()
        {
            Console.WriteLine("Name of existing fleets:");
            foreach (Fleet unit in ListFleets)
            {
                Console.WriteLine("[]{0}", unit.FleetName);
            }
        }

        public Fleet GetFleetWithName(string name)
        {
            foreach (Fleet unit in ListFleets)
            {
                if (unit.FleetName == name)
                {
                    return unit;
                }
            }
            return null;
        }
    }
}
