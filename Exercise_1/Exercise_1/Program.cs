﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Person person1 = new Person();

            Console.WriteLine("What is your name?");
            person1.FirstName = Console.ReadLine();

            Console.WriteLine("What is your Last name?");
            person1.LastName = Console.ReadLine();

            Console.WriteLine("How old are you?");
            person1.Age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is your occupation?");
            person1.Occupation = Console.ReadLine();

            Console.WriteLine("When is your birthdate?");
            person1.Birthdate = float.Parse(Console.ReadLine());

            Console.WriteLine("When is your BirthYear?");
            person1.BirthYear = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is your weight?");
            person1.Weight = float.Parse(Console.ReadLine());

            Console.Clear();

            //Checking of Age
            person1.CheckAge(person1.Age);

            Console.WriteLine();

            //Saving of Information
            person1.SaveInfo();
         
            Console.ReadKey();
            
        }
        
    }

}
