﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;
        public string Occupation;
        public float Birthdate;
        public int BirthYear;
        public float Weight;
        

        public Person()
        {

        }

        public void CheckAge(int age)
        {
            Age = age;
            if(age < 18 && age > 0)
            {
                Console.WriteLine("You're not allowed to access this page!");
                return;
            }
            if (age > 17 && age < 71)
            {
                Console.WriteLine("Welcome! You are free to access any page");
                Console.WriteLine();
                Console.WriteLine("My name is {0}, {1}. My birthyear is {2} and I am {3} years old. My occupation is {4}.", 
                    LastName, FirstName,BirthYear, Age, Occupation);
                return;
            }
            else
            {
                Console.WriteLine("Error!Age is out of Range");
                return;
            }
        }

        public void SaveInfo()
        {
            string choice;
            Console.WriteLine("Do you wan't to save this information? y/n");
            choice = Console.ReadLine();
            switch (choice)
            {
                case "y":
                case "Y":
                    Console.WriteLine("Thank you!");
                    break;
                case "n":
                case "N":
                    Console.WriteLine("Thank you!Data was not saved!");
                    break;
            }
        }
    }
}
