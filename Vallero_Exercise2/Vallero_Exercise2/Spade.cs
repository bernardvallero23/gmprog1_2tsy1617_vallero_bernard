﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Spade : Suit
    {
        public Spade()
        {
            SuitType = suit.Spade;
            SuitMultiplier = 8;
            InitCards();
        }
    }
}
