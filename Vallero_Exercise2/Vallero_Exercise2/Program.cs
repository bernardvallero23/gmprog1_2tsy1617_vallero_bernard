﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            DeckManager deckManager = new DeckManager();

            for (;;)
            {
                Console.WriteLine("Draw a card:");
                deckManager.Draw(0);
                deckManager.Draw(1);
                Console.ReadLine();
                if (deckManager.GetMoney() <= 0)
                {
                    break;
                }
            }
            Console.WriteLine("GAME OVER!");
            Console.ReadLine();
        }
    }
}
