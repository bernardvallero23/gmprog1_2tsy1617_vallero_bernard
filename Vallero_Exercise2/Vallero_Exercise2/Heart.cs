﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Heart : Suit
    {
        public Heart()
        {
            SuitType = suit.Heart;
            SuitMultiplier = 5;
            InitCards();
        }
    }
}
