﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Diamond : Suit
    {
        public Diamond()
        {
            SuitType = suit.Diamond;
            SuitMultiplier = 3;
            InitCards();
        }
    }
}
