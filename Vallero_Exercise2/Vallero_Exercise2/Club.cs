﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Club : Suit
    {
        public Club() {
            SuitType = suit.Club;
            SuitMultiplier = 1;
            InitCards();
        }
    }
}
