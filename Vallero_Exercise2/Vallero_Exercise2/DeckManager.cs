﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class DeckManager
    {
        Club clubs = new Club();
        Diamond diamonds = new Diamond();
        Heart hearts = new Heart();
        Spade spades = new Spade();
        List<Suit> Deck = new List<Suit>();

        int Money = 100;

        Random rnd = new Random();

        public DeckManager() {
            Deck.Add(clubs);
            Deck.Add(diamonds);
            Deck.Add(hearts);
            Deck.Add(spades);
            Console.WriteLine("You got {0} coins!", Money);
        }

        public void Draw(int i) {

            int deck_idx = rnd.Next(0, Deck.Count());
            Suit suitDraw = Deck[deck_idx];

            Card draw = suitDraw.PickCard();

            int value = suitDraw.GetCardValue(draw);
            switch (i) {
                case 0:
                    Money -= value;
                    Console.WriteLine("You got {0} {1}, it costs {2}!", suitDraw.GetSuitType(), draw.CardName, value);
                    break;
                case 1:
                    Money += value;
                    Console.WriteLine("You got {0} {1}, you gained {2}!", suitDraw.GetSuitType(), draw.CardName, value);
                    break;
            }
            

            
            Console.WriteLine("Money left is {0}", Money);
           
        }

        public int GetMoney() {
            return Money;
        }
    }
}
