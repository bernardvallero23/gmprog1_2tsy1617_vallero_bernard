﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vallero_Exercise2
{
    class Suit
    {

        public enum suit { Club, Spade, Diamond, Heart, def };
        protected suit SuitType = suit.def;

        protected int SuitMultiplier;
        List<Card> SuitCards = new List<Card>();

        Random rnd = new Random();

        public Card PickCard() {
            int idx = rnd.Next(0, SuitCards.Count());
            return SuitCards[idx];
        }

        public int GetCardValue(Card card)
        {
            int value = SuitMultiplier * card.Price;
            return value;
        }

        public string GetSuitType() {
            return SuitType.ToString();
        }

        private void PrintCards() {
            Console.Write(SuitType + "s ");
            foreach (Card card in SuitCards) {
                Console.Write(card.CardName + " ");
            }
            Console.Write("\n");
        }

        protected void InitCards() {
            Card A = new Card();
            A.Price = 10;
            A.CardName = "A";
            SuitCards.Add(A);

            for (int r = 0; r < 3; r++) {
                Card K = new Card();
                K.Price = 5;
                switch (r) {
                    case 0:
                        K.CardName = "K";
                        break;
                    case 1:
                        K.CardName = "Q";
                        break;
                    case 2:
                        K.CardName = "J";
                        break;
                }
                
                SuitCards.Add(K);
            }


            for (int i = 1; i < 10; i++) {
                Card c = new Card();
                c.Price = 2;
                int Name = i + 1;
                c.CardName = Name.ToString();
                SuitCards.Add(c);
            }

            PrintCards();
        }

    }
}
